﻿using BCP_MODELS.Models;

namespace BCP_DATA_ACCES.Repositories
{
    public interface IProveedorRepository
    {
        public IEnumerable<Proveedor> GetProveedors(int? Id);

    }
}
