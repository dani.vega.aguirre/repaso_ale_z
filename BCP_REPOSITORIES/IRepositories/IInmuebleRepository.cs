﻿using BCP_MODELS.Models;
namespace BCP_DATA_ACCES.Repositories
{
    public interface IInmuebleRepository
    {
        public IEnumerable<Inmueble> GetInmuebles(int? Id);
    }
}
