﻿using BCP_MODELS.Models;
namespace BCP_DATA_ACCES.Repositories
{
    public interface IRepresentanteLegalRepository
    {
        public IEnumerable<RepresentanteLegal> GetRepresentantes(int? Id);

    }
}
