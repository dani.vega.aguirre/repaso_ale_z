﻿namespace BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;

public interface IAuthRepository
{
    public User? GetUser(string userName);

    public void RegisterUser(User user);

    public void UpdateUser(User user);
}
