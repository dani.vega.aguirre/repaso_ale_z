﻿using BCP_CONTRATOS_DMVA.Models;
using BCP_MODELS.Models;

namespace BCP_DATA_ACCES.Repositories
{
    public interface IContratosRepository
    {
        public IEnumerable<Contrato> GET(ParametersContrato parametersContrato);
        public Contrato POST(PostContratos postContratos);
        
    }
}
