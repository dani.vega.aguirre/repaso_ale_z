﻿using BCP_MODELS.Models;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace BCP_DATA_ACCES.Repositories
{
    public class ProveedorRepository:IProveedorRepository
    {
        private readonly string connectionString;

        public ProveedorRepository(SQLConnection sqlConnection)
        {
            this.connectionString = sqlConnection.ConnectionString;
        }

        public IEnumerable<Proveedor> GetProveedors(int? Id)
        {
            using IDbConnection db = new SqlConnection(this.connectionString);
            var parameters = new DynamicParameters();
            parameters.Add("@ID", Id != default ? Id : null, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@PATERNO", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@MATERNO", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@NOMBRES", null, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@DOCUMENTO", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@DOMICILIO", null, DbType.String, ParameterDirection.Input);
            var storeProc = "GET_PROVEEDOR";
            IEnumerable<Proveedor> proveedors = db.Query<Proveedor>(storeProc, parameters, commandType: CommandType.StoredProcedure);
            return proveedors;
        }
    }
}
