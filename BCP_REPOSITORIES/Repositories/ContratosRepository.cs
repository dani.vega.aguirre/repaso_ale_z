﻿using BCP_CONTRATOS_DMVA.Models;
using BCP_MODELS.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;


namespace BCP_DATA_ACCES.Repositories
{
    public class ContratosRepository: IContratosRepository
    {
        private readonly string connectionString;

        public ContratosRepository(SQLConnection sqlConnection)
        {
            this.connectionString = sqlConnection.ConnectionString;
        }

        public IEnumerable<Contrato> GET(ParametersContrato parametersContrato)
        {
            using IDbConnection db = new SqlConnection(this.connectionString);
            var parameters = new DynamicParameters();
            parameters.Add("@ID", parametersContrato.Id, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@FECHA", parametersContrato.Fecha, DbType.Date, ParameterDirection.Input);
            parameters.Add("@REPRESENTANTELEGAL", parametersContrato.RepresentanteLegal, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@PROVEEDOR", parametersContrato.Proveedor, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@INMUEBLE", parametersContrato.Inmueble, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@IMPORTE", parametersContrato.Importe, DbType.Double, ParameterDirection.Input);
            parameters.Add("@CUENTA", parametersContrato.Cuenta, DbType.String, ParameterDirection.Input);
            var storeProc = "GET_CONTRATOS";
            IEnumerable<Contrato> contratos = db.Query<Contrato>(storeProc, parameters, commandType: CommandType.StoredProcedure);
            return contratos;
        }

        public Contrato POST(PostContratos postContratos)
        {
            using IDbConnection db = new SqlConnection(this.connectionString);
            var parameters = new DynamicParameters();
            parameters.Add("@CODIGO_CONTRATO", postContratos.CODIGO_CONTRATO, DbType.String, ParameterDirection.Input,10);
            parameters.Add("@REPRESENTANTE_LEGAL", postContratos.REPRESENTANTE_LEGAL, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@PROVEEDOR", postContratos.PROVEEDOR, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@INMUEBLE", postContratos.INMUEBLE, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@IMPORTE", postContratos.IMPORTE, DbType.Double, ParameterDirection.Input);
            parameters.Add("@LITERAL", postContratos.LITERAL, DbType.String, ParameterDirection.Input,200);
            parameters.Add("@CUENTA", postContratos.CUENTA, DbType.String, ParameterDirection.Input,20);
            parameters.Add("@NUMEROMESES", postContratos.NUMEROMESES, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@FECHAINICIALARRENDAMIENTO", postContratos.FECHAINICIALARRENDAMIENTO, DbType.Date, ParameterDirection.Input);
            parameters.Add("@FECHAFINALARRENDAMIENTO", postContratos.FECHAFINALARRENDAMIENTO, DbType.Date, ParameterDirection.Input);
            parameters.Add("@FECHATENOR", postContratos.FECHATENOR, DbType.Date, ParameterDirection.Input);
            parameters.Add("@MES", postContratos.MES, DbType.String, ParameterDirection.Input,30);
            parameters.Add("@ANIO", postContratos.ANIO, DbType.String, ParameterDirection.Input,4);
            var storeProc = "POST_CONTRATOS";
            Contrato contrato = db.Query<Contrato>(storeProc, parameters, commandType: CommandType.StoredProcedure).FirstOrDefault()!;
            return contrato;
        }
    }
}
