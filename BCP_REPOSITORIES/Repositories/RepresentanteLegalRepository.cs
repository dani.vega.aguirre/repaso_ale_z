﻿using BCP_MODELS.Models;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace BCP_DATA_ACCES.Repositories
{
    public class RepresentanteLegalRepository:IRepresentanteLegalRepository
    {
        private readonly string connectionString;

        public RepresentanteLegalRepository(SQLConnection sqlConnection)
        {
            this.connectionString = sqlConnection.ConnectionString;
        }

        public IEnumerable<RepresentanteLegal> GetRepresentantes(int? Id)
        {
            using IDbConnection db = new SqlConnection(this.connectionString);
            var parameters = new DynamicParameters();
            parameters.Add("@ID", Id != default ? Id : null, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@PATERNO", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@MATERNO", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@NOMBRES", null, DbType.Int16, ParameterDirection.Input);
            var storeProc = "GET_REPRESENTANTE_LEGAL";
            IEnumerable<RepresentanteLegal> representanteLegals = db.Query<RepresentanteLegal>(storeProc, parameters, commandType: CommandType.StoredProcedure);
            return representanteLegals;
        }
    }
}
