﻿using BCP_MODELS.Models;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace BCP_DATA_ACCES.Repositories
{
    public class InmuebleRepository : IInmuebleRepository
    {
        private readonly string connectionString;

        public InmuebleRepository(SQLConnection sqlConnection) 
        {
            this.connectionString = sqlConnection.ConnectionString;
        }
        public IEnumerable<Inmueble> GetInmuebles(int? Id)
        {
            using IDbConnection db = new SqlConnection(this.connectionString);
            var parameters = new DynamicParameters();
            parameters.Add("@ID", Id!=default?Id:null, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@DIRECCION", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@CIUDAD", null, DbType.String, ParameterDirection.Input);
            parameters.Add("@SUPERFICIE", null, DbType.Int16, ParameterDirection.Input);
            parameters.Add("@NUMERODIRECCION", null, DbType.String, ParameterDirection.Input);
            var storeProc = "GET_INMUEBLES";
            IEnumerable<Inmueble> inmuebles = db.Query<Inmueble>(storeProc, parameters, commandType: CommandType.StoredProcedure);
            return inmuebles;
        }
    }
}
