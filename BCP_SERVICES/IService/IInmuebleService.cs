﻿using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public interface IInmuebleService
    {
        public IEnumerable<Inmueble> GetInmuebles(int? id);
    }
}
