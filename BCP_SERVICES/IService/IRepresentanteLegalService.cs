﻿using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public interface IRepresentanteLegalService
    {
        public IEnumerable<RepresentanteLegal> GetRepresentanteLegals(int? id);
    }
}
