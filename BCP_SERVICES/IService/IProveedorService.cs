﻿using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public interface IProveedorService
    {
        public IEnumerable<Proveedor> GetProveedors(int? id);
    }
}
