﻿using BCP_MODELS.Models;
namespace BCP_SERVICES.Service
{
    public interface IAuthService
    {
        public User RegisterUser(SignUpRequest userR);

        public LoginResponse Login(LoginRequest login, RefreshToken refreshToken);
    }
}
