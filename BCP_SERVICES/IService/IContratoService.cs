﻿using Aspose.Pdf;
using BCP_CONTRATOS_DMVA.Models;
using BCP_MODELS.Models;
namespace BCP_SERVICES.Service;

public interface IContratoService
{
    public IEnumerable<Contrato> Get(ParametersContrato parametersContrato);
    public string GetPdf(ParametersContrato parametersContrato);
    public Contrato Post(PostContratos postContratos);

}
