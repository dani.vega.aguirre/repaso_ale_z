﻿using Aspose.Pdf;
using BCP_CONTRATOS_DMVA.Models;
using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;
using BCP_MODELS.Const;
using BCP_SERVICES.Service;
using FluentValidation.Results;
using System.Security.Cryptography;
using System;

namespace BCP_SERVICES.Service
{
    public class ContratoService : IContratoService
    {
        private readonly IContratosRepository contratosRepository;
        private readonly IInmuebleService inmuebleService;
        private readonly IRepresentanteLegalService representanteLegalService;
        private readonly IProveedorService proveedorService;

        public ContratoService(IContratosRepository contratosRepository, IInmuebleService inmuebleService, IRepresentanteLegalService representanteLegalService, IProveedorService proveedorService)
        {
            this.contratosRepository = contratosRepository;
            this.inmuebleService = inmuebleService;
            this.proveedorService = proveedorService;
            this.representanteLegalService = representanteLegalService;
        }

        public IEnumerable<Contrato> Get(ParametersContrato parametersContrato)
        {
            return this.contratosRepository.GET(parametersContrato);
        }

        public string GetPdf(ParametersContrato parametersContrato)
        {
            var contrato = this.contratosRepository.GET(parametersContrato).FirstOrDefault();
            if(contrato == default)
                throw new Exception($"No Existe ningun Contrato con el id : {parametersContrato.Id}");
            //Logica que obtenga los datos de la BD
            //Convertir información a HTML
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(CreateHtml(contrato));
            //doc.Save(AppDomain.CurrentDomain.BaseDirectory + "Template\\invoice1.pdf");
            byte[] data = doc.Save();
            var result = Convert.ToBase64String(data);
            doc.Close();
            //{status = "ok", data = "result"};
            return result;
        }

        public Contrato Post(PostContratos postContratos)
        {
            PostContratoValidator validator = new PostContratoValidator(this.inmuebleService, this.representanteLegalService, this.proveedorService);
            ValidationResult result = validator.Validate(postContratos);
            if (result.Errors.Any())
            {
                string errorsString = string.Join(" ", result.Errors);
                throw new ArgumentException(errorsString);
            }
            return this.contratosRepository.POST(postContratos);
        }

        public string CreateHtml(Contrato contrato)
        {
            string html = $"<img src={Const.img}>" +
                $"<h4>Extraido de la Base de Datos</h4>" +
                $"</br>" +
                $"<p> ID: {contrato.ID} </br>" +
                $" CODIGO_CONTRATO: {contrato.CODIGO_CONTRATO} </br>" +
                $" REPRESENTANTE LEGAL: {contrato.NOMBRES_LEGAL} {contrato.PATERNO_LEGAL} {contrato.MATERNO_LEGAL}</br>" +
                $" PROVEEDOR: {contrato.NOMBRES_PROVEEDOR} {contrato.PATERNO_PROVEEDOR} {contrato.MATERNO_PROVEEDOR} </br>" +
                $" INMUEBLE EN CALLE: {contrato.DIRECCION_INMUEBLE}</br>" +
                $" CON {contrato.SUPERFICIE} M² </br>" +
                $" EN LA CIUDAD DE {contrato.CIUDAD} </br>" +
                $" CON UN IMPORTE DE {contrato.IMPORTE} </br>" +
                $"</p>";

            return html;
        }
    }
}
