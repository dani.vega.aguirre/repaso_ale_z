﻿using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public class InmuebleService : IInmuebleService
    {
        private readonly IInmuebleRepository inmuebleRepository;
        public InmuebleService(IInmuebleRepository inmuebleRepository)
        {
            this.inmuebleRepository = inmuebleRepository;
        }
        public IEnumerable<Inmueble> GetInmuebles(int? id)
        {
            return this.inmuebleRepository.GetInmuebles(id);
        }
    }
}
