﻿namespace BCP_SERVICES.Service;


using FluentValidation.Results;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using ValidationFailure = FluentValidation.Results.ValidationFailure;
using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;

public class AuthService : IAuthService
{
    private readonly IAuthRepository authRepository;
    private readonly AuthSettings authSettings;

    public AuthService(IAuthRepository authRepository, AuthSettings authSettings)
    {
        this.authRepository = authRepository;
        this.authSettings = authSettings;
    }

    public User RegisterUser(SignUpRequest userR)
    {
        using var hmac = new HMACSHA512();
        var passwordSalt = hmac.Key;
        var passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(userR.Password));
        User user = new (userR.UserName, passwordHash, passwordSalt, userR.Role);
        this.authRepository.RegisterUser(user);
        return user;
    }

    public LoginResponse Login(LoginRequest login, RefreshToken refreshToken)
    {
        AuthValidator validator = new(this.authRepository);
        ValidationResult result = validator.Validate(login);
        if (result.Errors.Any())
        {
            string errorsString = string.Join(" ", result.Errors);
            throw new ArgumentException(errorsString);
        }
        User user = this.GetUser(login.Username) !;
        this.SetRefreshToken(user, refreshToken);
        return new LoginResponse(this.CreateToken(user));
    }

    public string CreateToken(User user)
    {
        List<Claim> claims = new ()
        {
            new Claim(ClaimTypes.Name, user.Username),
            new Claim(ClaimTypes.Role, user.Role.ToString())
        };
        var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8
            .GetBytes(this.authSettings.Secret));

        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
        var token = new JwtSecurityToken(
            claims: claims,
            expires: DateTime.Now.ToLocalTime().AddMinutes(60),
            signingCredentials: creds);

        var jwt = new JwtSecurityTokenHandler().WriteToken(token);
        return jwt;
    }

    public void SetRefreshToken(User user, RefreshToken newRefreshToken)
    {
        user.RefreshToken = newRefreshToken.Token;
        user.TokenExpires = newRefreshToken.Expires;
        user.TokenCreated = newRefreshToken.Created;
        this.authRepository.UpdateUser(user);
    }

    public User? GetUser(string userName)
    {
        return this.authRepository.GetUser(userName);
    }

}