﻿using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public class RepresentanteLegalService : IRepresentanteLegalService
    {
        private readonly IRepresentanteLegalRepository representanteLegalRepository;
        public RepresentanteLegalService(IRepresentanteLegalRepository representanteLegalRepository)
        {
            this.representanteLegalRepository = representanteLegalRepository;
        }
        public IEnumerable<RepresentanteLegal> GetRepresentanteLegals(int? Id)
        {
            return this.representanteLegalRepository.GetRepresentantes(Id);
        }
    }
}
