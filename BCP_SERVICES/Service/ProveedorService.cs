﻿using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;

namespace BCP_SERVICES.Service
{
    public class ProveedorService : IProveedorService
    {
        private readonly IProveedorRepository proveedorRepository;
        public ProveedorService(IProveedorRepository proveedorRepository)
        {
            this.proveedorRepository = proveedorRepository;
        }
        public IEnumerable<Proveedor> GetProveedors(int? id)
        {
            return this.proveedorRepository.GetProveedors(id);
        }
    }
}
