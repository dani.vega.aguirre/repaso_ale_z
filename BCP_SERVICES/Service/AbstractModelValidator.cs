﻿using FluentValidation;
using FluentValidation.Results;

namespace BCP_SERVICES.Service
{
    public class AbstractModelValidator<TModel> : AbstractValidator<TModel>
        where TModel : class, new()
    {
        /// <summary>
        /// This method allows executing and handling the validations of the a Model.
        /// </summary>
        /// <param name="model">The type model to be validated.</param>
        /// <exception cref="ArgumentException">Throws exception if the model is invalid.</exception>
        public void ExecuteAndHandleValidation(in TModel model)
        {
            ValidationResult validationResult = Validate(model);
            if (validationResult.Errors.Any())
            {
                string errorsString = string.Join(" ", validationResult.Errors);
                throw new ArgumentException(errorsString);
            }
        }
    }
}
