﻿using BCP_CONTRATOS_DMVA.Models;
using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;
using FluentValidation;

namespace BCP_SERVICES.Service
{
    public class PostContratoValidator : AbstractModelValidator<PostContratos>
    {
        public PostContratoValidator(IInmuebleService inmuebleService, IRepresentanteLegalService representanteLegalService, IProveedorService proveedorService)
        {
            this.RuleFor(contrato => contrato.INMUEBLE)
                .NotEmpty()
                .WithMessage("'{PropertyName}' = '{PropertyValue}' must not be empty.")
                .Must((contrato, inmueble) => inmuebleService.GetInmuebles(inmueble).Count() != 0)
                .WithMessage("'{PropertyName}' is not valid.");

            this.RuleFor(contrato => contrato.PROVEEDOR)
                .NotEmpty()
                .WithMessage("'{PropertyName}' = '{PropertyValue}' must not be empty.")
                .Must((contrato, proveedor) => proveedorService.GetProveedors(proveedor).Count() != 0)
                .WithMessage("'{PropertyName}' is not valid.");
            
            this.RuleFor(contrato => contrato.REPRESENTANTE_LEGAL)
                .NotEmpty()
                .WithMessage("'{PropertyName}' = '{PropertyValue}' must not be empty.")
                .Must((contrato, representanteLegal) => representanteLegalService.GetRepresentanteLegals(representanteLegal).Count() != 0)
                .WithMessage("'{PropertyName}' is not valid.");

            this.RuleFor(contrato => contrato.IMPORTE)
                .NotEmpty().WithMessage("'{PropertyName}' = '{PropertyValue}' must not be empty.");
            
        }
    }
}
