﻿namespace BCP_SERVICES.Service;

using FluentValidation;
using BCP_MODELS.Models;
using BCP_DATA_ACCES.Repositories;
using System.Security.Cryptography;

public class AuthValidator : AbstractValidator<LoginRequest>
{
    public AuthValidator(IAuthRepository authRepository)
    {
        this.RuleFor(user => user.Username)
            .NotEmpty().WithMessage("'{PropertyName}' mustn't empty")
            .MaximumLength(15).WithMessage("'{PropertyName}' must contain less than 15 letters.")
            .Matches("^[A-Za-z0-9 ]+$").WithMessage("'{PropertyName}' must not contain special characters.")
            .Must(username => authRepository.GetUser(username) != null)
            .WithMessage("Invalid Credential");

        this.RuleFor(user => user.Password)
            .NotEmpty().WithMessage("'{PropertyName}' mustn't empty")
            .MinimumLength(6).WithMessage("'{PropertyName}' must contain at least 6 characters.")
            .MaximumLength(50).WithMessage("'{PropertyName}' must contain at most 50 characters.")
            .WithErrorCode("Protected");

        this.RuleFor(user => user)
            .Must(user => this.VerifyPasswordHash(authRepository.GetUser(user.Username) !, user.Password))
            .WithMessage("Invalid Credential");
    }

    private bool VerifyPasswordHash(User user, string password)
    {
        if (user == null)
        {
            return false;
        }

        using (var hmac = new HMACSHA512(user.PasswordSalt))
        {
            var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            return computeHash.SequenceEqual(user.PasswordHash);
        }
    }
}
