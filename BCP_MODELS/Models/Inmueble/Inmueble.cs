﻿namespace BCP_MODELS.Models
{
    public class Inmueble
    {
        public int ID { get; set; }
        public string DIRECCION { get; set; } = string.Empty;
        public string CIUDAD { get; set; } = string.Empty;
        public int SUPERFICIE { get; set; }
        public string NUMERODIRECCION { get; set; } = string.Empty;
    }
}
