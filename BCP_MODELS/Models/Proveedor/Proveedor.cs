﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCP_MODELS.Models
{
    public class Proveedor
    {
        public int ID { get; set; }
        public string PATERNO { get; set; } = string.Empty;
        public string MATERNO { get; set; } = string.Empty;
        public string NOMBRES { get; set; } = string.Empty;
        public string DOCUMENTO { get; set; } = string.Empty;
        public string DOMICILIO { get; set; } = string.Empty;
    }
}

