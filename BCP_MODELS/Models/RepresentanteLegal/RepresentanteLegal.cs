﻿namespace BCP_MODELS.Models
{
    public class RepresentanteLegal
    {
        public string PATERNO{ get; set; } = string.Empty;
        public string MATERNO{ get; set; } = string.Empty;
        public string NOMBRES{ get; set; } = string.Empty;
        public string TESTIMONIO { get; set; } = string.Empty;
        public DateTime FECHAINICIAL { get; set; }
        public DateTime FECHAFINAL { get; set; }
        public DateTime FECHATESTIMONIO { get; set; }
        public int NUMERONOTARIA { get; set; }
    }
}
