﻿namespace BCP_MODELS.Models;
public class LoginResponse
{
    public string JwtToken { get; } = string.Empty;

    public LoginResponse(string jwtToken)
    {
        this.JwtToken = jwtToken;
    }
}
