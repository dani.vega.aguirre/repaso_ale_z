﻿namespace BCP_MODELS.Models;

using BCP_MODELS.Enums;
public class SignUpRequest
{
    public string UserName { get; set; } = string.Empty;

    public string Password { get; set; } = string.Empty;

    public Role Role { get; set; } = Role.Guest;
}
