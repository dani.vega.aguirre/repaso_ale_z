﻿using BCP_MODELS.Enums;
namespace BCP_MODELS.Models;
public class User
{
    public User(string username, byte[] passwordHash, byte[] passwordSalt, Role role)
    {
        this.Username = username;
        this.PasswordHash = passwordHash;
        this.PasswordSalt = passwordSalt;
        this.Role = role;
    }
    public int ID { get; set; }

    public string? Username { get; set; }

    public byte[]? PasswordHash;

    public byte[]? PasswordSalt;

    public Role Role { get; set; }

    public string? RefreshToken { get; set; }

    public DateTime TokenCreated { get; set; }

    public DateTime TokenExpires { get; set; }
}