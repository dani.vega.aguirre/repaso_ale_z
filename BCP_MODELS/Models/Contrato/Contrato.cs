﻿namespace BCP_MODELS.Models
{
    public class Contrato
    {
        public int ID { get; set; }
        public string CODIGO_CONTRATO { get; set; } = string.Empty;
		public string PATERNO_LEGAL { get; set; } = string.Empty;
		public string MATERNO_LEGAL { get; set; } = string.Empty;
		public string NOMBRES_LEGAL { get; set; } = string.Empty;
		public string TESTIMONIO { get; set; } = string.Empty;
		public DateTime FECHAINICIAL { get; set; }
		public DateTime FECHAFINAL { get; set; }
		public DateTime FECHATESTIMONIO { get; set; }
		public int NUMERONOTARIA { get; set; }
		public string PATERNO_PROVEEDOR { get; set; } = string.Empty;
		public string MATERNO_PROVEEDOR { get; set; } = string.Empty;
		public string NOMBRES_PROVEEDOR { get; set; } = string.Empty;
		public string DOCUMENTO_PROVEEDOR { get; set; } = string.Empty;
		public string DOMICILIO_PROVEEDOR { get; set; } = string.Empty;
		public string DIRECCION_INMUEBLE { get; set; } = string.Empty;
		public string CIUDAD { get; set; } = string.Empty;
		public int SUPERFICIE { get; set; }
		public string NUMERODIRECCION { get; set; } = string.Empty;
		public double IMPORTE { get; set; }
		public string LITERAL { get; set; } = string.Empty;
        public string CUENTA { get; set; } = string.Empty;
        public int NUMEROMESES { get; set; }
		public DateTime FECHAINICIALARRENDAMIENTO { get; set; }
		public DateTime FECHAFINALARRENDAMIENTO { get; set; } 
		public DateTime FECHATENOR { get; set; }
		public string MES { get; set; } = string.Empty;
        public string ANIO { get; set; } = string.Empty;
    }
}
