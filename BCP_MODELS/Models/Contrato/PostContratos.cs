﻿namespace BCP_CONTRATOS_DMVA.Models
{
    public class PostContratos
    {
        public string CODIGO_CONTRATO { get; set; } = string.Empty;
        public int REPRESENTANTE_LEGAL { get; set; }
        public int PROVEEDOR { get; set; }
        public int INMUEBLE { get; set; }
        public double IMPORTE { get; set; }
        public string LITERAL { get; set; } = string.Empty;
        public string CUENTA { get; set; } = string.Empty;
        public int NUMEROMESES { get; set; }
        public DateTime FECHAINICIALARRENDAMIENTO { get; set; }
        public DateTime FECHAFINALARRENDAMIENTO { get; set; }
        public DateTime FECHATENOR { get; set; }
        public string MES { get; set; } = string.Empty;
        public string ANIO { get; set; } = string.Empty;
    }
}
