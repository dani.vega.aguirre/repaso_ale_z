﻿
namespace BCP_MODELS.Models
{
    public class ParametersContrato
    {
        public int? Id{ get; set; }
        public DateTime? Fecha{ get; set; }
        public int? RepresentanteLegal { get; set; }
        public int? Proveedor { get; set; }
        public int? Inmueble { get; set; }
        public double? Importe { get; set; }
        public string? Cuenta { get; set; }

    }
}
