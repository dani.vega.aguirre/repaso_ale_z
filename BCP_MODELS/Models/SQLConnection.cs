﻿namespace BCP_MODELS.Models
{
    public class SQLConnection
    {
        public string? Database { get; init; }

        public string? Host { get; init; }

        public string? User { get; init; }

        public string? Password { get; init; }

        public string ConnectionString => $"Server={this.Host ?? string.Empty};Database={this.Database ?? string.Empty};User Id={this.User ?? string.Empty};Password={this.Password ?? string.Empty};";
    }
}
