﻿namespace BCP_MODELS.Enums;

public enum Role
{
    Guest,
    Staff,
    Coordinator
}
