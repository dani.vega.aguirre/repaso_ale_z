using BCP_DATA_ACCES.Repositories;
using BCP_MODELS.Models;
using BCP_REPOSITORIES;
using BCP_SERVICES.Service;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "BCP_OPERACIONES_DMVA", Version = "v1" });
});
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder =>
    {
        builder.WithOrigins("*").AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
    });
});

SQLConnection sqlSetup = builder.Configuration.GetSection("SQLConnection").Get<SQLConnection>();

builder.Services.AddScoped<IContratosRepository>(x => new ContratosRepository(sqlSetup));
builder.Services.AddScoped<IContratoService, ContratoService>();
builder.Services.AddScoped<IInmuebleRepository>(x => new InmuebleRepository(sqlSetup));
builder.Services.AddScoped<IInmuebleService, InmuebleService>();
builder.Services.AddScoped<IProveedorRepository>(x => new ProveedorRepository(sqlSetup));
builder.Services.AddScoped<IProveedorService, ProveedorService>();
builder.Services.AddScoped<IRepresentanteLegalRepository>(x => new RepresentanteLegalRepository(sqlSetup));
builder.Services.AddScoped<IRepresentanteLegalService, RepresentanteLegalService>();
builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());


string secret = builder.Configuration.GetSection("AuthSettings:Secret").Value;
//builder.Services.AddBearerJWTAuthentication(secret);
builder.Services.AddSingleton(new AuthSettings(secret));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
