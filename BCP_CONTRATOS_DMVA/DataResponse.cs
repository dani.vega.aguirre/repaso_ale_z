﻿using BCP_CONTRATOS_DMVA.ExceptionHandler;

namespace BCP_CONTRATOS_DMVA
{
    public class DataResponse
        <TModel>
    {
        public Error? Error { get; set; }
        public TModel? Data { get; set; }
    }
}
