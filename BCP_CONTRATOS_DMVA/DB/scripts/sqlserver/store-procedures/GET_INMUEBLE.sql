CREATE PROCEDURE GET_INMUEBLES 
@ID INT,
@DIRECCION VARCHAR(500),
@CIUDAD VARCHAR(30),
@SUPERFICIE INT,
@NUMERODIRECCION VARCHAR(10)
AS BEGIN
SELECT
    *
FROM
  INMUEBLE I
WHERE
	(@ID is NULL OR I.ID = @ID) AND
	(@DIRECCION is NULL OR I.DIRECCION = @DIRECCION) AND
	(@CIUDAD is NULL OR I.CIUDAD = @CIUDAD) AND
	(@SUPERFICIE is NULL OR I.SUPERFICIE = @SUPERFICIE) AND
	(@NUMERODIRECCION is NULL OR I.NUMERODIRECCION = @NUMERODIRECCION)
END
