CREATE PROCEDURE PUT_CLIENTE 
@Id INT,
@Id_Empresa INT,
@Estado INT,
@Id_Usuario INT AS BEGIN
UPDATE
  Cliente 
SET
  Id_Empresa = @Id_Empresa,
  Id_Usuario = @Id_Usuario,
  Estado = @Estado
WHERE
  Id = @Id
SELECT
  C.Id,
	C.Id_Empresa,
	C.Id_Usuario,
	E.Name AS Estado
FROM
  Cliente C
  INNER JOIN Estados E ON E.Id = C.Estado
WHERE
  C.Id = @Id
END