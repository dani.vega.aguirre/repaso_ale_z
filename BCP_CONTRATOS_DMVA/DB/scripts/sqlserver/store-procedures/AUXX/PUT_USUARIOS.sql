CREATE PROCEDURE PUT_USUARIOS 
@Id INT,
@Nombres VARCHAR(50),
@Apellidos VARCHAR(50),
@Correo VARCHAR(50),
@Genero BIT,
@Celular VARCHAR(50),
@NombreUsuario VARCHAR(50),
@Contraseña VARCHAR(50),
@Estado BIT AS BEGIN
UPDATE
  Usuarios 
SET
    Nombres = @Nombres,
    Apellidos = @Apellidos,
    Correo= @Correo,
    Genero= @Genero,
    Celular= @Celular,
    NombreUsuario= @NombreUsuario,
    Contraseña= @Contraseña,
    Estado= @Estado
WHERE
  Id_usuarios = @Id
SELECT
  *
FROM
  Usuarios U
WHERE
  U.Id_usuarios = @Id
END 