CREATE PROCEDURE POST_USUARIOS
@Nombres VARCHAR(50),
@Apellidos VARCHAR(50),
@Correo VARCHAR(50),
@Genero BIT,
@Celular VARCHAR(50),
@NombreUsuario VARCHAR(50),
@Contraseña VARCHAR(50)
AS BEGIN
INSERT INTO
  Usuarios (
    Nombres,
    Apellidos,
    Correo,
    Genero,
    Celular,
    NombreUsuario,
    Contraseña,
    Estado
  )
VALUES
  (
    @Nombres,
    @Apellidos,
    @Correo,
    @Genero,
    @Celular,
    @NombreUsuario,
    @Contraseña,
    1
  )
SELECT
  *
FROM
  Usuarios U
WHERE
  U.Id_usuarios = SCOPE_IDENTITY()
END