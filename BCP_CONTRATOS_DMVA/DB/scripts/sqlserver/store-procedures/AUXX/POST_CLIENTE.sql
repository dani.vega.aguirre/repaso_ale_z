CREATE PROCEDURE POST_CLIENTE 
@Id_Empresa INT,
@Id_Usuario INT AS BEGIN
INSERT INTO
  Cliente (Id_Empresa, Id_Usuario,Estado)
VALUES
  (@Id_Empresa, @Id_Usuario,1)
SELECT
  C.Id,
	C.Id_Empresa,
	C.Id_Usuario,
	E.Name AS Estado
FROM
  Cliente C
  INNER JOIN Estados E ON E.Id = C.Estado
WHERE
  C.Id = SCOPE_IDENTITY()
END