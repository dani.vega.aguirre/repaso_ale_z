CREATE PROCEDURE GET_CLIENTE @Estado INT AS BEGIN
SELECT
  C.Id,
	C.Id_Empresa,
	C.Id_Usuario,
	E.Name AS Estado
FROM
  Cliente C
  INNER JOIN Estados E ON E.Id = C.Estado
WHERE
	@Estado is NULL OR C.Estado = @Estado
END