namespace BCP_CONTRATOS_DMVA;

using BCP_CONTRATOS_DMVA.ExceptionHandler;
using BCP_CONTRATOS_DMVA.Models;
using BCP_MODELS.Models;
using BCP_SERVICES.Service;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;

[ApiController]
[Route("api/[controller]")]
public class AuthController : ControllerBase
{
    private readonly IAuthService authService;

    public AuthController(IAuthService authService)
    {
        this.authService = authService;
    }

    [HttpPost("login")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Authentication([FromBody] LoginRequest loginRequest)
    {
        var refreshToken = this.CreateRefreshToken();
        DataResponse<LoginResponse> response = new DataResponse<LoginResponse>();
        try
        {
            response.Data = this.authService.Login(loginRequest, refreshToken);
        }
        catch (Exception ex)
        {
            response.Error = new Error(ex);
        }
        return this.Ok(response);
    }

    [HttpPost("sign-up")]
    public IActionResult RegisterStudent(SignUpRequest request)
    {
        DataResponse<User> response = new DataResponse<User>();
        try
        {
            response.Data = this.authService.RegisterUser(request);
        }
        catch (Exception ex)
        {
            response.Error = new Error(ex);
        }
        return this.Ok(response);
    }

    private RefreshToken CreateRefreshToken()
    {
        var refreshToken = new RefreshToken
        {
            Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
            Expires = DateTime.Now.AddDays(1),
            Created = DateTime.Now,
        };
        var cookieOptions = new CookieOptions
        {
            HttpOnly = true,
            Expires = refreshToken.Expires,
        };
        return refreshToken;
    }
}