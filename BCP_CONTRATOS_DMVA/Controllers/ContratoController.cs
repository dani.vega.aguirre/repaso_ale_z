using Microsoft.AspNetCore.Mvc;
using BCP_SERVICES.Service;
using BCP_MODELS.Models;
using BCP_CONTRATOS_DMVA.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Razor;
using BCP_CONTRATOS_DMVA.ExceptionHandler;
using Aspose.Pdf;

namespace BCP_CONTRATOS_DMVA.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContratoController : ControllerBase
    {
        private readonly IContratoService contratoService;
        private readonly IMapper mapper;

        public ContratoController(IContratoService contratoService, IMapper mapper)
        {
            this.contratoService = contratoService;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get( int? Id, DateTime? Fecha, int? RepresentanteLegal, int? Proveedor, int? Inmueble, double? Importe, string? Cuenta) {
            DataResponse<IEnumerable<Contrato>> response = new DataResponse<IEnumerable<Contrato>>();
            try {
                ParametersContrato parametersContrato = this.mapper.Map<ParametersContrato>((Id,Fecha, RepresentanteLegal, Proveedor, Inmueble, Importe, Cuenta));
                response.Data = this.contratoService.Get(parametersContrato);
            } catch(Exception ex)
            {
                response.Error = new Error(ex);
            }
            return this.Ok(response);

        }

        [HttpGet]
        [Route("{Id:int}")]
        public IActionResult GetPdf([FromRoute] int Id)
        {
            DataResponse<string> response = new DataResponse<string>();
            try
            {
                ParametersContrato parametersContrato = this.mapper.Map<ParametersContrato>(Id);
                response.Data = this.contratoService.GetPdf(parametersContrato);
            }
            catch (Exception ex)
            {
                response.Error = new Error(ex);
            }
            return this.Ok(response);

        }

        [HttpPost (Name = "POST_CONTRATO")]
        public IActionResult Post(PostContratos postContratos)
        {
            DataResponse<Contrato> response = new DataResponse<Contrato>();
            try
            {
                response.Data = this.contratoService.Post(postContratos);
            }
            catch (Exception ex)
            {
                response.Error = new Error(ex);
            }
            return this.Ok(response);
        }
    }
}