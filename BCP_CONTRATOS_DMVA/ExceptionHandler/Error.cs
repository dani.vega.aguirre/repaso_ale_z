﻿namespace BCP_CONTRATOS_DMVA.ExceptionHandler
{
    public class Error
    {
        public string MessageError { get; set; } = string.Empty;

        public string? Code { get; set; } = string.Empty;

        public Error(Exception e)
        {
            MessageError = e.Message;
            Code = e.Source;
        }
    }
}
