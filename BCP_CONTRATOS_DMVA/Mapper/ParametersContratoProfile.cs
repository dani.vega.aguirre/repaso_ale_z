﻿using AutoMapper;
using BCP_MODELS.Models;
using System.Security.Claims;

namespace BCP_CONTRATOS_DMVA
{
    public class ParametersContratoProfile : Profile
    {
        public ParametersContratoProfile()
        {
            this.CreateMap<(int? Id, DateTime? Fecha, int? RepresentanteLegal, int? Proveedor, int? Inmueble, double? Importe, string? Cuenta), ParametersContrato>()
                .ForMember(parameter => parameter.Id, opts => opts.MapFrom(input => input.Id))
                .ForMember(parameter => parameter.Fecha, opts => opts.MapFrom(input => input.Fecha))
                .ForMember(parameter => parameter.RepresentanteLegal, opts => opts.MapFrom(input => input.RepresentanteLegal))
                .ForMember(parameter => parameter.Proveedor, opts => opts.MapFrom(input => input.Proveedor))
                .ForMember(parameter => parameter.Inmueble, opts => opts.MapFrom(input => input.Inmueble))
                .ForMember(parameter => parameter.Importe, opts => opts.MapFrom(input => input.Importe))
                .ForMember(parameter => parameter.Cuenta, opts => opts.MapFrom(input => input.Cuenta));

            this.CreateMap<int, ParametersContrato>()
                .ForMember(parameter => parameter.Id, opts => opts.MapFrom(input => input))
                .ForMember(parameter => parameter.Fecha, opts => opts.Ignore())
                .ForMember(parameter => parameter.RepresentanteLegal, opts => opts.Ignore())
                .ForMember(parameter => parameter.Proveedor, opts => opts.Ignore())
                .ForMember(parameter => parameter.Inmueble, opts => opts.Ignore())
                .ForMember(parameter => parameter.Importe, opts => opts.Ignore())
                .ForMember(parameter => parameter.Cuenta, opts => opts.Ignore());
        }


    }
}
